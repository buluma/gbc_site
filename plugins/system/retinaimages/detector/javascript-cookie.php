<?php
/**
 *  @package    Retina Images - pixelRatioDetector - JavascriptCookie
 *  @author     Josef Břehovský / EasyJoomla.org {@link http://www.easysjoomla.org}
 *  @copyright  2014 Josef Břehovský / EasyJoomla.org
 *  @authEmail  josef.brehovsky@easyjoomla.org
 *  @version    1.3
 *  @link       http://www.easyjoomla.org/less-compiler Documentation of LESS Compiler plugin
 *  @license    http://www.gnu.org/licenses/gpl-3.0.html GNU GPL 3.0
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
defined('_JEXEC') or die('Restricted access');

class pixelRatioDetector {

	public static function detect() {
		$pixelRatio = 1;

		if( isset( $_COOKIE["pixel_ratio"] ) ){
			$pixelRatio = $_COOKIE["pixel_ratio"];

		} else {
			$detect_script = "
<script type=\"text/javascript\">
writeCookie();
function writeCookie()
{
	var pixelRatio = 1;
	if( window.devicePixelRatio ){
		pixelRatio = window.devicePixelRatio;
	}
	expiry = new Date();
	expiry.setTime(expiry.getTime()+(30*24*60*60*1000));
	document.cookie = 'pixel_ratio=' + pixelRatio + '; expires=' + expiry.toGMTString() + ';' + document.cookie;
	if (pixelRatio != 1 && document.cookie.indexOf('pixel_ratio') != -1) {
		location = '".JURI::current()."';
	}
}
</script>";

			$body = JResponse::getBody();
			$body = preg_replace(
				'@(<head\b[^>]*>)@i',
				'$1'.$detect_script,
				$body
			);

			JResponse::setBody($body);
		}

		return  $pixelRatio;
	}

}