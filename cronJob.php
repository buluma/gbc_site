<?php
error_reporting(0);
//import the config job
require_once('configuration.php');
$config = new JConfig();
$connection = mysql_connect($config->host,$config->user, $config->password) or die(mysql_error());
$database = mysql_select_db($config->db) or die(mysql_error());
//database
$queryContactsAll = mysql_query("SELECT * FROM " .$config->dbprefix. "contact_us") or die(mysql_error());
$queryContactsYear = mysql_query("SELECT * FROM " .$config->dbprefix. "contact_us WHERE YEAR(date_current) = YEAR(CURRENT_DATE)") or die(mysql_error());
$queryContactsMonth = mysql_query("SELECT * FROM " .$config->dbprefix. "contact_us WHERE YEAR(date_current) = YEAR(CURRENT_DATE) AND MONTH(date_current) = MONTH(CURRENT_DATE)") or die(mysql_error());
$queryContactsWeek = mysql_query("SELECT * FROM " .$config->dbprefix. "contact_us WHERE YEAR(date_current) = YEAR(CURRENT_DATE) AND WEEK(date_current) = WEEK(CURRENT_DATE)") or die(mysql_error());
//registrations
//$queryRegAll = mysql_query("SELECT * FROM " .$config->dbprefix. "users") or die(mysql_error());
//$queryRegYear = mysql_query("SELECT * FROM " .$config->dbprefix. "users WHERE YEAR(registerDate) = YEAR(CURRENT_DATE)") or die(mysql_error());
//$queryRegMonth = mysql_query("SELECT * FROM " .$config->dbprefix. "users WHERE YEAR(registerDate) = YEAR(CURRENT_DATE) AND MONTH(registerDate) = MONTH(CURRENT_DATE)") or die(mysql_error());
//$queryRegWeek = mysql_query("SELECT * FROM " .$config->dbprefix. "users WHERE YEAR(registerDate) = YEAR(CURRENT_DATE) AND WEEK(registerDate) = WEEK(CURRENT_DATE)") or die(mysql_error());
//submission
//$querySubAll = mysql_query("SELECT * FROM " .$config->dbprefix. "users WHERE submitted = '1'") or die(mysql_error());
//$querySubYear = mysql_query("SELECT * FROM " .$config->dbprefix. "users WHERE submitted = '1' AND YEAR(date_submission) = YEAR(CURRENT_DATE)") or die(mysql_error());
//$querySubMonth = mysql_query("SELECT * FROM " .$config->dbprefix. "users WHERE submitted = '1' AND YEAR(date_submission) = YEAR(CURRENT_DATE) AND MONTH(date_submission) = MONTH(CURRENT_DATE)") or die(mysql_error());
//$querySubWeek = mysql_query("SELECT * FROM " .$config->dbprefix. "users WHERE submitted = '1' AND YEAR(date_submission) = YEAR(CURRENT_DATE) AND WEEK(date_submission) = WEEK(CURRENT_DATE)") or die(mysql_error());
//get all the enquiries
//subject
$subject = 'GBC Website : Weekly Update';
//message
$message = '
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>' . $subject . '</title>
</head>
</head>
<body>
<div id="wrapper">';
$message .= '<table border="1" cellspacing="0" cellpadding="3" bordercolor="#cccccc" style="border-collapse:collapse;font-size:12px;font-family:arial;width:100%">';
$message .= '<tr><td colspan="6" style="background-color:#007B00;color:white;font-size:14px;padding:8px">Weekly update for the GBC Website</td></tr>';
$message .= '<tr><td style="padding:5px" colspan="6"><strong>Survey Entries</strong></td></tr>';
$message .= '<tr><td style="padding:5px" colspan="5">Total number of contact enquiries submitted</td><td align="center">' .mysql_num_rows($queryContactsAll). '</td></tr>';
$message .= '<tr><td style="padding:5px" colspan="5">Total number of contact enquiries year to date</td><td align="center">' .mysql_num_rows($queryContactsYear). '</td></tr>';
$message .= '<tr><td style="padding:5px" colspan="5">Total number of contact enquiries for this month (' .date('M'). ')</td><td align="center">' .mysql_num_rows($queryContactsMonth). '</td></tr>';
$message .= '<tr><td style="padding:5px" colspan="5">Total number of contact enquiries this week (' .date('W'). ')</td><td align="center">' .mysql_num_rows($queryContactsWeek). '</td></tr>';
//registrations
//$message .= '<tr><td style="padding:5px" colspan="5"><strong>Registrations</strong></td></tr>';
//$message .= '<tr><td style="padding:5px" colspan="4">Total number of registrations</td><td align="center">' .mysql_num_rows($queryRegAll). '</td></tr>';
//$message .= '<tr><td style="padding:5px" colspan="4">Total number of registrations year to date</td><td align="center">' .mysql_num_rows($queryRegYear). '</td></tr>';
//$message .= '<tr><td style="padding:5px" colspan="4">Total number of registrations for this month (' .date('M'). ')</td><td align="center">' .mysql_num_rows($queryRegMonth). '</td></tr>';
//$message .= '<tr><td style="padding:5px" colspan="4">Total number of registrations this week (' .date('W'). ')</td><td align="center">' .mysql_num_rows($queryRegWeek). '</td></tr>';

//submissions
//$message .= '<tr><td style="padding:5px" colspan="5"><strong>Submissions</strong></td></tr>';
//$message .= '<tr><td style="padding:5px" colspan="4">Total number of submissions</td><td align="center">' .mysql_num_rows($querySubAll). '</td></tr>';
//$message .= '<tr><td style="padding:5px" colspan="4">Total number of submissions year to date</td><td align="center">' .mysql_num_rows($querySubYear). '</td></tr>';
//$message .= '<tr><td style="padding:5px" colspan="4">Total number of submissions for this month (' .date('M'). ')</td><td align="center">' .mysql_num_rows($querySubMonth). '</td></tr>';
//$message .= '<tr><td style="padding:5px" colspan="4">Total number of submissions this week (' .date('W'). ')</td><td align="center">' .mysql_num_rows($querySubWeek). '</td></tr>';
//contact us enquiries dateils for the week
if(mysql_num_rows($queryContactsWeek) > 0){
$message .= '<tr><td style="padding:5px" colspan="6"><strong>Survey Entries for this week details</strong></td></tr>';
$message .= '<tr><td style="padding:5px"><strong>Name</strong></td><td style="padding:5px"><strong>Email Address</strong></td><td style="padding:5px"><strong>Phone</strong></td><td style="padding:5px"><strong>Department</strong></td><td style="padding:5px"><strong>Message</strong></td><td style="padding:5px"><strong>Date Submitted</strong></td></tr>';
while($contacts = mysql_fetch_object($queryContactsWeek)){
	$message .= '<tr><td style="padding:5px">' .$contacts->name. '</td><td style="padding:5px">' .$contacts->email. '</td><td style="padding:5px">' .$contacts->phone. '</td><td style="padding:5px">' .$contacts->dept. '</td><td style="padding:5px">' .ucwords($contacts->message). '</td><td style="padding:5px">' .$contacts->date_current. '</td></tr>';
	}
}
//registrations dateils for the week
if(mysql_num_rows($queryRegWeek) > 0){
$message .= '<tr><td style="padding:5px" colspan="5"><strong>Registrations for this week details</strong></td></tr>';
$message .= '<tr><td style="padding:5px" colspan="2">Name</td><td style="padding:5px">University</td><td style="padding:5px">Team leader</td><td style="padding:5px">Team leader\'s email</td></tr>';
while($registrations = mysql_fetch_object($queryRegWeek)){
	$message .= '<tr><td style="padding:5px" colspan="2">' .$registrations->name. '</td><td style="padding:5px">' .$registrations->university_name. '</td><td style="padding:5px">' .$registrations->team_leader_name. '</td><td style="padding:5px">' .$registrations->email. '</td></tr>';
	}
}
//hits
$getHits = mysql_query("SELECT * FROM gbc_hits WHERE WEEK(date_hits) = WEEK(CURRENT_DATE) AND YEAR(date_hits) = YEAR(CURRENT_DATE)");//hits
$message .= '
		<tr>
			<td style="padding:8px;background-color:#007B00;font-size:11px;color:#ffffff;font-weight:bold;" colspan="6">Weekly Hits</td>
		</tr>
		<tr>
				<td style="padding:8px;font-size:11px;color:#000;font-weight:bold;" colspan="5">Hits for the past week (Week - ' .date('W'). ')</td>
				<td style="padding:8px;font-size:11px;color:#000;font-weight:bold;width:100px;" align="center">' .mysql_num_rows($getHits). '</td>
		</tr>';
$message .= '</table></div>';
$message .= '</body>';
$message .= '</html>';
//email headers
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=us-ascii' . "\r\n";
$headers .= 'From: GBC Website : Weekly Update<info@gbc.co.ke>'. "\r\n";
$headers .= 'Cc: support@gbc.co.ke'. "\r\n";
//$headers .= 'Bcc: kmuturi@gbc.co.ke'. "\r\n";
$headers .= '1\r\nX-MSMail-Priority: High' . "\r\n";
//receipient
$to = 'clients@gbc.co.ke';
echo $message;
mail($to,$subject,$message,$headers);
?>
